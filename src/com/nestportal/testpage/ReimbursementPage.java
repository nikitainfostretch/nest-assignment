package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ReimbursementPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.Reimbursement.Reimburse")
	private QAFExtendedWebElement btnReimbursement;

	@FindBy(locator = "btn.MyExpense.Reimburse")
	private QAFExtendedWebElement btnMyExpense;

	@FindBy(locator = "btn.NewExpense.Reimburse")
	private QAFExtendedWebElement btnnewExpense;

	@FindBy(locator = "txt.title.form")
	private QAFExtendedWebElement txttitle;

	@FindBy(locator = "select.project.form")
	private QAFExtendedWebElement selectproject;

	@FindBy(locator = "select.projectname.form")
	private QAFExtendedWebElement selectprojectname;

	@FindBy(locator = "select.expense.form")
	private QAFExtendedWebElement selectexpense;

	@FindBy(locator = "txt.currency.form")
	private QAFExtendedWebElement txtcurrency;

	@FindBy(locator = "select.category.form")
	private QAFExtendedWebElement selectcategory;

	@FindBy(locator = "txt.expenseamount.form")
	private QAFExtendedWebElement txtexpenseamount;

	@FindBy(locator = "btn.submit.expense")
	private QAFExtendedWebElement btnsubmit;

	@FindBy(locator = "btn.browse.form")
	private QAFExtendedWebElement btnbrowse;

	@FindBy(locator = "img.uploadfile.form")
	private QAFExtendedWebElement imguploadfile;

	@FindBy(locator = "btn.teamreimbursement.expense")
	private QAFExtendedWebElement btnteamreimbursement;

	@FindBy(locator = "btn.projectname.expense")
	private QAFExtendedWebElement btnprojectname;

	@FindBy(locator = "txt.reimbursement.form")
	private QAFExtendedWebElement txtreimbursement;

	@FindBy(locator = "btn.Approve.form")
	private QAFExtendedWebElement btnApprove;

	@FindBy(locator = "input.addcomments.expense")
	private QAFExtendedWebElement inputaddcomments;

	public QAFExtendedWebElement getBtnReimbursement() {
		return btnReimbursement;
	}

	public QAFExtendedWebElement getBtnMyExpense() {
		return btnMyExpense;
	}

	public QAFExtendedWebElement getBtnnewExpense() {
		return btnnewExpense;
	}

	public QAFExtendedWebElement getSelectproject() {
		return selectproject;
	}

	public QAFExtendedWebElement getSelectexpense() {
		return selectexpense;
	}

	public QAFExtendedWebElement getTxtcurrency() {
		return txtcurrency;
	}

	public QAFExtendedWebElement getSelectcategory() {
		return selectcategory;
	}

	public QAFExtendedWebElement getTxtexpenseamount() {
		return txtexpenseamount;
	}

	public QAFExtendedWebElement getBtnsubmit() {
		return btnsubmit;
	}

	public QAFExtendedWebElement getBtnbrowse() {
		return btnbrowse;
	}

	public QAFExtendedWebElement getImguploadfile() {
		return imguploadfile;
	}

	public QAFExtendedWebElement getBtnteamreimbursement() {
		return btnteamreimbursement;
	}

	public QAFExtendedWebElement getBtnprojectname() {
		return btnprojectname;
	}

	public QAFExtendedWebElement getTxtreimbursement() {
		return txtreimbursement;
	}

	public QAFExtendedWebElement getBtnApprove() {
		return btnApprove;
	}

	public QAFExtendedWebElement getInputaddcomments() {
		return inputaddcomments;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
	}

	public void redirecttomyexpense() {
		waitForPageToLoad();
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		btnReimbursement.waitForPresent(30000);

		executor.executeScript("arguments[0].click()", btnReimbursement);

		btnMyExpense.waitForPresent(30000);
		executor.executeScript("arguments[0].click()", btnMyExpense);

		btnnewExpense.waitForPresent(30000);
		executor.executeScript("arguments[0].click()", btnnewExpense);

		QAFTestBase.pause(30000);
	}

	public void fillexpenseform(String title, String project, String projectname,
			String expenseTitle, int expenseAmount) {
		waitForPageToLoad();
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		txttitle.waitForPresent(3000);
		txttitle.sendKeys(
				ConfigurationManager.getBundle().getString("Reimbursement.title"));

		selectproject.waitForPresent(5000);
		executor.executeScript("arguments[0].click()", selectproject);
		selectproject.sendKeys(
				ConfigurationManager.getBundle().getString("Reimbursement.project"));

		selectprojectname.waitForPresent();
		executor.executeScript("arguments[0].click()", selectprojectname);

		btnprojectname.sendKeys(
				ConfigurationManager.getBundle().getString("Reimbursement.projectname"));

		selectexpense.waitForPresent(4000);
		executor.executeScript("arguments[0].click()", selectexpense);
		selectexpense.sendKeys(
				ConfigurationManager.getBundle().getString("Reimbursement.expensetitle"));

		selectcategory.waitForPresent();
		executor.executeScript("arguments[0].click()", selectcategory);

		txtexpenseamount.sendKeys(ConfigurationManager.getBundle()
				.getString("Reimbursement.expenseamount"));

		executor.executeScript("arguments[0].click()", btnsubmit);

		QAFTestBase.pause(30000);

	}

	public void uploadfile() {
		waitForPageToLoad();

		QAFWebElement element =
				new QAFExtendedWebElement("xpath=(//input[@type='file'])[1]");
		element.sendKeys("D:\\QAS\\workspace\\NestPortal\\placeholder.jpg");

		QAFTestBase.pause(10000);

	}

	public void verifyupload() {
		if (imguploadfile.isPresent()) {
			System.out.println("image is upload suceesfully");
		} else System.out.println("image is not uploaded");
	}

	public void teamReimbursementPage() {

		waitForPageToLoad();
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		btnReimbursement.waitForPresent(30000);

		executor.executeScript("arguments[0].click()", btnReimbursement);

		btnteamreimbursement.waitForPresent(30000);
		executor.executeScript("arguments[0].click()", btnteamreimbursement);

		txtreimbursement.waitForPresent(30000);
		executor.executeScript("arguments[0].click()", txtreimbursement);

		CommonUtils.waitforloaderimg();

		executor.executeScript("arguments[0].scrollIntoView();", btnApprove);

		// executor.executeScript("arguments[0].click()",inputaddcomments);
		CommonUtils.clickUsingActionOnElement(inputaddcomments);

		inputaddcomments.sendKeys("Approve");
		executor.executeScript("arguments[0].click()", btnApprove);

		QAFTestBase.pause(60000);
	}

}
