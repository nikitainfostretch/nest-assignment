package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class OtherRequestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.request.homepage")
	private QAFExtendedWebElement btnrequest;
	
	@FindBy(locator = "img.request.requestdetail")
	private QAFExtendedWebElement requestimg;
	
	@FindBy(locator = "icon.viewdetail.requestpage")
	private QAFExtendedWebElement viewdetailicon;
	
	@FindBy(locator = "btn.selectemployee.requestpage")
	private QAFExtendedWebElement btnnominee;
	
	@FindBy(locator ="txt.search.requestpage")
	private QAFExtendedWebElement inputnominee;
	
	@FindBy(locator ="select.employee.searchedlist")
	private QAFExtendedWebElement selectnominee;
	
	@FindBy(locator ="btn.search.requestpage")
	private QAFExtendedWebElement searchbutton;

	@FindBy(locator ="btn.nomineedetail.requestpage")
	private QAFExtendedWebElement searchnominee;

	public QAFExtendedWebElement getBtnrequest() {
		return btnrequest;
	}
		
	public QAFExtendedWebElement getRequestimg() {
		return requestimg;
	}

	public QAFExtendedWebElement getViewdetailicon() {
		return viewdetailicon;
	}
	
	public QAFExtendedWebElement getBtnnominee() {
		return btnnominee;
	}
	

	public QAFExtendedWebElement getInputnominee() {
		return inputnominee;
	}
	

	public QAFExtendedWebElement getSelectnominee() {
		return selectnominee;
	}
	

	public QAFExtendedWebElement getSearchbutton() {
		return searchbutton;
	}
	
	
	public QAFExtendedWebElement getSearchnominee() {
		return searchnominee;
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
		public void otherrequest() {

			btnrequest.waitForPresent(15000);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", btnrequest);


			  Actions actions = new Actions(driver);
		   
			  QAFWebElement moveonmenu = requestimg;
			  QAFWebElement subcategory = viewdetailicon;
			
			  moveonmenu.waitForVisible();
			  actions.moveToElement(moveonmenu).build().perform();
			
			  subcategory.waitForVisible();
//			  actions.moveToElement(subcategory).click().perform();
			  executor.executeScript("arguments[0].click();", subcategory);
			  
			  QAFTestBase.pause(10000);
		
	}

		public void searchNomination(String nomineeName) {
			
			waitForPageToLoad();
			
			btnnominee.waitForPresent(30000);

			CommonUtils.waitforloaderimg();
			CommonUtils.clickUsingJavaScript(btnnominee);

			inputnominee.sendKeys(ConfigurationManager.getBundle().getString("Search.nominee"));

			CommonUtils.clickUsingJavaScript(selectnominee);
			
			CommonUtils.waitforloaderimg();

			CommonUtils.clickUsingJavaScript(btnnominee);
			searchbutton.waitForPresent(30000);

			CommonUtils.clickUsingJavaScript(searchbutton);
			
			QAFTestBase.pause(30000);
		}
		
		
		public void verifyUserPresent() {
			waitForPageToLoad();
			if(searchnominee.isDisplayed()== true) {
				System.out.println("user is present on request list");
			}
			else
				System.out.println("user is not present on request list");
		}
}
