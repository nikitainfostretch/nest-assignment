package com.nestportal.testpage;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class InvalidLoginPage extends WebDriverBaseTestPage<WebDriverTestPage>  {

	@FindBy(locator = "btn.login.loginpage")
	private QAFExtendedWebElement btnLogin;
	
	@FindBy(locator = "txt.login.invalid")
	private QAFExtendedWebElement txtalert;
	
	public QAFExtendedWebElement getBtnLogin() {
		return btnLogin;
	}
	
	public QAFExtendedWebElement getTxtalert() {
		return txtalert;
	}
	

	
	public void doInvalidLogin() {
		btnLogin.waitForPresent(4000);
		btnLogin.submit();
		waitForPageToLoad();
	}
	
	public void verifyAlerttext() {
		
		String AlertText = txtalert.getText();
		Reporter.log(AlertText);
		waitForPageToLoad();
		
		
	   waitForAjaxToComplete();
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
}
