package com.nestportal.testpage;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TrainingCalenderPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.menu.homescreen")
	private QAFExtendedWebElement btnmenu;

	@FindBy(locator = "btn.training.menu")
	private QAFExtendedWebElement btntraining;

	@FindBy(locator = "btn.trainingcalender.training")
	private QAFExtendedWebElement btntrainingcalender;

	@FindBy(locator = "txt.trainingname.navigate")
	private List<QAFWebElement> txttrainingname;

	@FindBy(locator = "btn.register.training")
	private QAFExtendedWebElement btnregister;
	
	@FindBy(locator = "txt.pageTitle.trainingcalender")
	private QAFExtendedWebElement txtpageTitle;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public QAFExtendedWebElement getBtnmenu() {
		return btnmenu;
	}

	public QAFExtendedWebElement getBtntraining() {
		return btntraining;
	}

	public QAFExtendedWebElement getBtntrainingcalender() {
		return btntrainingcalender;
	}

	public List<QAFWebElement> getTxttrainingname() {
		return txttrainingname;
	}

	public QAFExtendedWebElement getBtnregister() {
		return btnregister;
	}
	

	public QAFExtendedWebElement getTxtpageTitle() {
		return txtpageTitle;
	}


	public void redirectToTrainingPage() throws InterruptedException {

		waitForPageToLoad();

		btnmenu.waitForPresent(30000);

		CommonUtils.clickUsingJavaScript(btnmenu);

		btntraining.waitForPresent(30000);

		CommonUtils.clickUsingJavaScript(btntraining);

		btntrainingcalender.waitForPresent(30000);

		CommonUtils.clickUsingJavaScript(btntrainingcalender);

		waitForPageToLoad();

		CommonUtils.waitforloaderimg();
		
		Thread.sleep(5000);
		
		txtpageTitle.waitForPresent(5000);
		waitForPageToLoad();

		List<QAFWebElement> tranings = getTxttrainingname();
		System.out.println("list size: " + tranings.size());
	
		System.out.println("=======list size::"+tranings.size());
		if ( tranings.size() > 0) {
			QAFWebElement firstElement = tranings.get(2);
			System.out.println("====first"+firstElement.isDisplayed());

			
			try {
//				CommonUtils.scrollUpToElement(firstElement);
				firstElement.waitForPresent(30000);
				//CommonUtils.clickUsingJavaScript(firstElement);
				CommonStep.waitForEnabled("txt.trainingname.navigate");
				CommonStep.click("txt.trainingname.navigate");
				  } catch (Exception e) {
				   e.printStackTrace();
				  }

		}
		waitForPageToLoad();
		QAFTestBase.pause(50000);
	}

	public void registerForTraining() {

		waitForPageToLoad();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("scroll(0,800);");
//		CommonUtils.scrollUpToElement(btnregister);
		if(btnregister.isPresent()) {

		btnregister.waitForPresent(30000);
		CommonUtils.clickUsingJavaScript(btnregister);
		}
		else {
			
			System.out.println("registeration is closed for this training");
		}
		QAFTestBase.pause(50000);
	}

}
