package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TrainingModulePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.training.menu")
	private QAFExtendedWebElement btnmenutraining;

	@FindBy(locator = "btn.trainings.navigation")
	private QAFExtendedWebElement btntrainings;

	@FindBy(locator = "txt.trainingsDetail.navigate")
	private QAFExtendedWebElement txttrainingsDetail;

	@FindBy(locator = "btn.browse.training")
	private QAFExtendedWebElement btnbrowse;

	@FindBy(locator = "btn.submit.training")
	private QAFExtendedWebElement btnsubmit;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public QAFExtendedWebElement getTxttrainingname() {
		return txttrainingsDetail;
	}

	public QAFExtendedWebElement getBtnmenutraining() {
		return btnmenutraining;
	}

	public QAFExtendedWebElement getBtntrainings() {
		return btntrainings;
	}

	public QAFExtendedWebElement getBtnbrowse() {
		return btnbrowse;
	}

	public void redirecttotraining() {

		waitForPageToLoad();
		btnmenutraining.waitForPresent(9000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnmenutraining);
		
		btntrainings.waitForPresent(4000);
		executor.executeScript("arguments[0].click();", btntrainings);
		
		txttrainingsDetail.waitForPresent(8000);
		executor.executeScript("arguments[0].click();", txttrainingsDetail);
		
		// btnbrowse.waitForVisible(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
				btnbrowse);
		waitForPageToLoad();
		QAFTestBase.pause(6000);

	}

	public void browsefile() {
		waitForPageToLoad();
	
		QAFWebElement element =
				new QAFExtendedWebElement("xpath=(//input[@type='file'])[1]");
		element.sendKeys("D:\\QAS\\workspace\\NestPortal\\placeholder.jpg");
		

		
		QAFTestBase.pause(10000);
	}

}
