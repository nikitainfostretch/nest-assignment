package com.nestportal.testpage;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class BrightSparkPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "btn.rnr.navigation")
	private QAFExtendedWebElement btnrnr;
	
	@FindBy(locator = "btn.nominate.navigation")
	private QAFExtendedWebElement btnnominate;
	
	@FindBy(locator = "btn.brightspark.navigation")
	private QAFExtendedWebElement btnbrightspark;
	
	@FindBy(locator = "btn.nominee.brightspark")
	private QAFExtendedWebElement btnnominee;
	
	@FindBy(locator ="input.nominee.search")
	private QAFExtendedWebElement inputnominee;
	
	@FindBy(locator ="select.nominee.searchedlist")
	private QAFExtendedWebElement selectnominee;
	
	@FindBy(locator ="btn.quarter.brightspark")
	private QAFExtendedWebElement btnquarter;
	
	@FindBy(locator ="btn.submit.brightspark")
	private QAFExtendedWebElement btnsubmit;
	
	@FindBy(locator ="txt.addcomment.brightspark")
	private QAFExtendedWebElement addcomment;
	
	@FindBy(locator = "btn.menu.homescreen")
	private QAFExtendedWebElement btnMenu ;
	
	@FindBy(locator = "btn.rnrRequest.rnr")
	private QAFExtendedWebElement btnrnrRequest ;

	@FindBy(locator = "btn.reward.rnr")
	private QAFExtendedWebElement btnrewardrnr ;
	
	@FindBy(locator = "btn.status.rnr")
	private QAFExtendedWebElement btnstatus ;
	

	@FindBy(locator = "select.reject.rnr")
	private QAFExtendedWebElement btnRejectrnr ;
	
	@FindBy(locator = "select.brightSpark.rnr")
	private QAFExtendedWebElement btnbrightSparkreward ;
	
	@FindBy(locator = "btn.reimbursement.rnr")
	private QAFExtendedWebElement btnreimbursement ;
	
	@FindBy(locator = "btn.search.rnr")
	private QAFExtendedWebElement btnSearch ;
	
	@FindBy(locator = "btn.rejecteduser.rnr")
	private QAFExtendedWebElement btnrejecteduser ;
	
	@FindBy(locator = "btn.fields.brightspark")
	private List<WebElement> btnfields ;

	@FindBy(locator = "btn.valueaddition.brightspark")
	private List<WebElement> valueaddition ;	
	
	@FindBy(locator = "btn.back.brightspark")
	private QAFExtendedWebElement btnback ;

	@FindBy(locator = "img.award.brightspark")
	private QAFExtendedWebElement imgaward ;
	
	@FindBy(locator = "select.card.brightspark")
	private QAFExtendedWebElement selectcard ;	
	
	@FindBy(locator = "btn.project.brightspark")
	private QAFExtendedWebElement btnproject ;
	
	@FindBy(locator = "input.situationfaced.brightspark")
	private QAFExtendedWebElement inputsituationfaced ;
	
	@FindBy(locator = "input.solutionprovided.brightspark")
	private QAFExtendedWebElement inputsolutionprovided ;
	
	@FindBy(locator = "input.benefitaccured.brightspark")
	private QAFExtendedWebElement inputbenefitaccured ;
	
	@FindBy(locator = "input.Rationale.brightspark")
	private QAFExtendedWebElement inputRationale ;

	@FindBy(locator = "btn.post.brightspark")
	private QAFExtendedWebElement btnpost ;
	
	@FindBy(locator = "btn.myrnr.navigation")
	private QAFExtendedWebElement btnmyrnr ;
		
	@FindBy(locator = "btn.myrewards.brightspark")
	private QAFExtendedWebElement btnmyrewards ;

	@FindBy(locator = "txt.nomineedeatil.rewarddetail")
	private QAFExtendedWebElement txtnomineedeatil ;
	
	@FindBy(locator = "txt.brightspark.rewarddetail")
	private QAFExtendedWebElement txtbrightspark ;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}


	public QAFExtendedWebElement getBtnmyrnr() {
		return btnmyrnr;
	}

	public QAFExtendedWebElement getBtnnominate() {
		return btnnominate;
	}

	public QAFExtendedWebElement getBtnbrightspark() {
		return btnbrightspark;
	}
	
	public QAFExtendedWebElement getBtnnominee() {
		return btnnominee;
	}
	

	public QAFExtendedWebElement getInputnominee() {
		return inputnominee;
	}
	

	public QAFExtendedWebElement getSelectnominee() {
		return selectnominee;
	}
	

	public QAFExtendedWebElement getBtnquarter() {
		return btnquarter;
	}


	public QAFExtendedWebElement getBtnsubmit() {
		return btnsubmit;
	}
	

	public QAFExtendedWebElement getAddcomment() {
		return addcomment;
	}

	public QAFExtendedWebElement getBtnMenu() {
		return btnMenu;
	}

	public QAFExtendedWebElement getBtnrnrRequest() {
		return btnrnrRequest;
	}

	public QAFExtendedWebElement getBtnrewardrnr() {
		return btnrewardrnr;
	}

	public QAFExtendedWebElement getBtnstatus() {
		return btnstatus;
	}

	public QAFExtendedWebElement getBtnRejectrnr() {
		return btnRejectrnr;
	}

	public QAFExtendedWebElement getBtnbrightSparkreward() {
		return btnbrightSparkreward;
	}
	
	public QAFExtendedWebElement getBtnreimbursement() {
		return btnreimbursement;
	}
	

	public QAFExtendedWebElement getBtnSearch() {
		return btnSearch;
	}
	
	public QAFExtendedWebElement getBtnrejecteduser() {
		return btnrejecteduser;
	}
	
	public List<WebElement> getBtnfields() {
		return btnfields;
	}
	
	public List<WebElement> getValueaddition() {
		return valueaddition;
	}
	
	
	public QAFExtendedWebElement getBtnback() {
		return btnback;
	}

	public QAFExtendedWebElement getImgaward() {
		return imgaward;
	}

	public QAFExtendedWebElement getBtnproject() {
		return btnproject;
	}

	public QAFExtendedWebElement getSelectcard() {
		return selectcard;
	}
	
	public QAFExtendedWebElement getInputsituationfaced() {
		return inputsituationfaced;
	}

	public QAFExtendedWebElement getInputsolutionprovided() {
		return inputsolutionprovided;
	}

	public QAFExtendedWebElement getInputbenefitaccured() {
		return inputbenefitaccured;
	}

	public QAFExtendedWebElement getInputRationale() {
		return inputRationale;
	}
	
	public QAFExtendedWebElement getBtnpost() {
		return btnpost;
	}

	public QAFExtendedWebElement getBtnmyrewards() {
		return btnmyrewards;
	}

	public QAFExtendedWebElement getBtnrnr() {
		return btnrnr;
	}
	
	public QAFExtendedWebElement getTxtnomineedeatil() {
		return txtnomineedeatil;
	}

	public QAFExtendedWebElement getTxtbrightspark() {
		return txtbrightspark;
	}
	
	public void brightspark() {
		
		waitForPageToLoad();
		
		btnrnr.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", 	btnrnr);
//		CommonUtils.waitforloaderimg();
//		btnrnr.click();
		
		btnnominate.waitForPresent(40000);
	    executor.executeScript("arguments[0].click();", 	btnnominate);
//	    CommonUtils.waitforloaderimg();
//	    btnnominate.click();
	    
	    btnbrightspark.waitForPresent(40000);
	    executor.executeScript("arguments[0].click();", 	btnbrightspark);
//	    CommonUtils.waitforloaderimg();
//	    btnbrightspark.click();
	    QAFTestBase.pause(30000);
	    
	}
	
	public void  SubmitDetails(String nomineeName,String projectName,String inputsituation,String inputsolution,String inputbenefit,String inputRational) {
	
		waitForPageToLoad();
		selectcard.click();

		btnnominee.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnnominee);

		inputnominee.sendKeys(ConfigurationManager.getBundle().getString("Search.nominee"));
		selectnominee.click();

		// Select project name

		btnproject.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", btnproject);

		inputnominee.sendKeys(ConfigurationManager.getBundle().getString("Search.projectName"));
		selectnominee.click();

		 inputsituationfaced.sendKeys(ConfigurationManager.getBundle().getString("text.textfield"));

		inputsolutionprovided.sendKeys(ConfigurationManager.getBundle().getString("text.textfield"));

		inputbenefitaccured.sendKeys(ConfigurationManager.getBundle().getString("text.textfield"));

		inputRationale.sendKeys(ConfigurationManager.getBundle().getString("text.textfield"));
		
		executor.executeScript("arguments[0].scrollIntoView();", btnpost);
		btnpost.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", btnpost);
//		btnpost.click();
		
		 QAFTestBase.pause(60000);
	}
	
	public void rnrPage() {
       waitForPageToLoad();
					  
		btnrnr.waitForPresent(40000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", 	btnrnr);
//		btnrnr.click();
//		CommonUtils.waitforloaderimg();
		
		executor.executeScript("arguments[0].click();", 	btnrnrRequest);
//		btnrnrRequest.waitForPresent(40000);
//		btnrnrRequest.click();
//		CommonUtils.waitforloaderimg();
		waitForPageToLoad();
	}
	
	public void applyFilter() {
		
		waitForPageToLoad();
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", 	btnstatus);
		
//		btnstatus.waitForPresent(15000);
//		btnstatus.click();
//		btnRejectrnr.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", 	btnRejectrnr);
		btnstatus.waitForPresent(2000);
		executor.executeScript("arguments[0].click();", 	btnstatus);
		
		waitForPageToLoad();
		
//		btnRejectrnr.click();
//		btnrewardrnr.waitForPresent(15000);
		
		executor.executeScript("arguments[0].click();", 	btnrewardrnr);
//		btnrewardrnr.click();
//		btnbrightSparkreward.waitForPresent(2000);
		executor.executeScript("arguments[0].click();", 	btnbrightSparkreward);
//		btnbrightSparkreward.click();
//		btnrewardrnr.click();
		btnrewardrnr.waitForPresent(2000);
		executor.executeScript("arguments[0].click();", 	btnrewardrnr);
		
		btnSearch.waitForPresent(2000);
		
		executor.executeScript("arguments[0].click();", 	btnSearch);
//		btnSearch.click();
	
		waitForPageToLoad();
		 QAFTestBase.pause(30000);
	}
	
	public void verifyPageDetail() {
		
		waitForPageToLoad();
		if(btnrejecteduser.isPresent()== true) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", 	btnrejecteduser);			
		}
		else {
			Reporter.log("no rejected brightSpark user found");
		}
		
		//Verify header element present
		String[] exp = {"HR Status","Manager Status","Manager","Nominated By","Posted Date","Reward Name","Project","Nominee"};
				
		
		List<WebElement> UiElements =  getBtnfields();
		System.out.println("list size: "+UiElements.size());
	
//		for (int i = 0; i < UiElements.size(); i++) {
//			System.out.println(
//					"element name in the list:" + UiElements.get(i).getText());		
//		}	
		for(WebElement we:UiElements)  
        {  
         for (int i=0; i<exp.length; i++){
             if (we.getText().equals(exp[i])){
             System.out.println("element "+i+" is Matched");
             } 
           }
        }
         
         //verify element on value addition
         String[] exp1 = {"Challenging situations faced","Solutions provided/Situation handling","Benefits accrued","Rationale for Nomination","Managers Comments"};
         
         List<WebElement> values =  getValueaddition();
 		System.out.println("list size: "+values.size());
		
 		for(WebElement web:values)  
        {  
         for (int i=0; i<exp1.length; i++){
             if (web.getText().equals(exp1[i])){
             System.out.println("element "+i+" is Matched for value addition");
             } 
           }
        }
 		
 		//Verify reward image
 		Assert.assertEquals(true, imgaward.isPresent());
 		//verify back button		
 		Assert.assertEquals(true, btnback.isPresent());
 		
		 QAFTestBase.pause(30000);
	}

	public void redirecttomyreward() {
		btnrnr.waitForPresent(40000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", 	btnrnr);
		executor.executeScript("arguments[0].click();", 	btnmyrnr);
		executor.executeScript("arguments[0].click();", 	btnmyrewards);
//		
//		executor.executeScript("arguments[0].click();", 	txtnomineedeatil);
//		executor.executeScript("arguments[0].click();",     txtbrightspark);
		QAFTestBase.pause(30000);
		
	}

	public void redirectToAwardDetailScreen() {
		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		txtnomineedeatil.waitForPresent(2000);
		executor.executeScript("arguments[0].click();", 	txtnomineedeatil);
		txtbrightspark.waitForPresent(2000);
		executor.executeScript("arguments[0].click();",     txtbrightspark);
	}


}

