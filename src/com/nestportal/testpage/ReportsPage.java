package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class ReportsPage extends WebDriverBaseTestPage<WebDriverTestPage> { 

	@FindBy(locator = "btn.rnr.reports")
	private QAFExtendedWebElement btnrnr;
	
	@FindBy(locator = "btn.reports.navigation")
	private QAFExtendedWebElement btnreports;
	
	@FindBy(locator = "btn.reward.reports")
	private QAFExtendedWebElement btnreward;
	
	@FindBy(locator = "btn.search.reports")
	private QAFExtendedWebElement btnsearch;
	
	@FindBy(locator = "btn.export.reports")
	private QAFExtendedWebElement btnexport;
	
	@FindBy(locator = "btn.selectreward.reports")
	private QAFExtendedWebElement btnselectreward;
	
	@FindBy(locator = "btn.menu.homescreen")
	private QAFExtendedWebElement btnmenu;

	public QAFExtendedWebElement getBtnrnr() {
		return btnrnr;
	}

	public QAFExtendedWebElement getBtnreports() {
		return btnreports;
	}

	public QAFExtendedWebElement getBtnreward() {
		return btnreward;
	}

	public QAFExtendedWebElement getBtnselectreward() {
		return btnselectreward;
	}

	
	public QAFExtendedWebElement getBtnsearch() {
		return btnsearch;
	}

	public QAFExtendedWebElement getBtnexport() {
		return btnexport;
	}


@Override
protected void openPage(PageLocator locator, Object... args) {
	// TODO Auto-generated method stub
	
}

public void redirectToReport() {
	waitForPageToLoad();
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	executor.executeScript("arguments[0].click();", 	btnrnr);
	executor.executeScript("arguments[0].click();", 	btnreports);

	waitForPageToLoad();
	executor.executeScript("arguments[0].click();", 	btnreward);
	btnselectreward.waitForPresent(40000);
	executor.executeScript("arguments[0].click();", 	btnselectreward);
	executor.executeScript("arguments[0].click();", 	btnreward);
	btnsearch.waitForPresent(40000);
	executor.executeScript("arguments[0].click();", 	btnsearch);
	
	CommonUtils.waitforloaderimg();
	executor.executeScript("arguments[0].scrollIntoView();",
			btnexport);
	executor.executeScript("arguments[0].click();", 	btnexport);
	
	QAFTestBase.pause(10000);
	
}
}