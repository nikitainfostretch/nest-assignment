package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class PatOnBackPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.rnr.navigation")
	private QAFExtendedWebElement btnrnr;

	@FindBy(locator = "btn.nominate.navigation")
	private QAFExtendedWebElement btnnominate;

	@FindBy(locator = "btn.patonback.navigation")
	private QAFExtendedWebElement btnpatonback;

	@FindBy(locator = "input.nominee.search")
	private QAFExtendedWebElement inputnominee;
	@FindBy(locator = "select.nominee.searchedlist")
	private QAFExtendedWebElement selectnominee;
	@FindBy(locator = "btn.nominee.patOnBack")
	private QAFExtendedWebElement btnnominee;

	@FindBy(locator = "btn.project.patonback")
	private QAFExtendedWebElement btnproject;

	@FindBy(locator = "input.contribution.patonback")
	private QAFExtendedWebElement inputcontribution;

	@FindBy(locator = "select.card.patonback")
	private QAFExtendedWebElement selectcard;

	@FindBy(locator = "btn.post.patonback")
	private QAFExtendedWebElement btnpost;
	
	@FindBy(locator = "btn.search.patonback")
	private QAFExtendedWebElement btnsearch;

	public QAFExtendedWebElement getBtnrnr() {
		return btnrnr;
	}

	public QAFExtendedWebElement getBtnnominate() {
		return btnnominate;
	}

	public QAFExtendedWebElement getBtnpatonback() {
		return btnpatonback;
	}

	public QAFExtendedWebElement getInputnominee() {
		return inputnominee;
	}

	public QAFExtendedWebElement getSelectnominee() {
		return selectnominee;
	}

	public QAFExtendedWebElement getBtnnominee() {
		return btnnominee;
	}

	public QAFExtendedWebElement getBtnproject() {
		return btnproject;
	}

	public QAFExtendedWebElement getInputcontribution() {
		return inputcontribution;
	}

	public QAFExtendedWebElement getSelectcard() {
		return selectcard;
	}

	public QAFExtendedWebElement getBtnpost() {
		return btnpost;
	}
	

	public QAFExtendedWebElement getBtnsearch() {
		return btnsearch;
	}
	
	@FindBy(locator = "btn.nomineedetail.patonback")
	private QAFExtendedWebElement btnnomineedetail;
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public void redirectonpatonback() {
		waitForPageToLoad();

		btnrnr.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnrnr);
		// CommonUtils.waitforloaderimg();
		// btnrnr.click();

		btnnominate.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", btnnominate);
		// CommonUtils.waitforloaderimg();
		// btnnominate.click();

		btnpatonback.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", btnpatonback);
		// CommonUtils.waitforloaderimg();
		// btnbrightspark.click();
		QAFTestBase.pause(30000);

	}

	public void submitPatOnBackForm(String nominee,String projectname,String addContribution) {

		waitForPageToLoad();

		selectcard.click();

		btnnominee.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnnominee);

		inputnominee.sendKeys(ConfigurationManager.getBundle().getString("nominee.patOnBack"));
		selectnominee.click();

		// Select project name

		btnproject.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", btnproject);

		inputnominee.sendKeys(ConfigurationManager.getBundle().getString("projectname.patOnBack"));
		selectnominee.click();

		// Add contribution

		inputcontribution.sendKeys(ConfigurationManager.getBundle().getString("text.textfield"));

		executor.executeScript("arguments[0].scrollIntoView();", btnpost);

		btnpost.click();
		QAFTestBase.pause(30000);
	}

	public void searchPatOnBackUser(String nominee) {
		waitForPageToLoad();
		
		btnnominee.waitForPresent(30000);

		CommonUtils.waitforloaderimg();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnnominee);
//		btnnominee.click();
		inputnominee.sendKeys(ConfigurationManager.getBundle().getString("nominee.patOnBack"));
//		selectnominee.click();
		executor.executeScript("arguments[0].click();", selectnominee);
		
		CommonUtils.waitforloaderimg();
//		btnnominee.click();
		executor.executeScript("arguments[0].click();", btnnominee);
		
		btnsearch.waitForPresent(30000);
//		btnsearch.click();
		executor.executeScript("arguments[0].click();", btnsearch);
		
		QAFTestBase.pause(30000);
		
	}

	public void verifyPatOnBackUser() {
		// TODO Auto-generated method stub
		waitForPageToLoad();
		if(btnnomineedetail.isPresent()== true) {
			System.out.println("user is present on request list");
		}
		else
			System.out.println("user is not present on request list");
	}

}
