package com.nestportal.testpage;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class MyPostPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.myPost.navigation")
	private QAFExtendedWebElement btnmypost;

	@FindBy(locator = "btn.post.navigation")
	private QAFExtendedWebElement btnpost;

	@FindBy(locator = "btn.posttitle.form")
	private List<WebElement> btnposttitle;

	@FindBy(locator = "btn.search.form")
	private QAFExtendedWebElement btnsearch;

	@FindBy(locator = "btn.reset.form")
	private QAFExtendedWebElement btnreset;

	@FindBy(locator = "txt.title.form")
	private QAFExtendedWebElement txttitle;

	@FindBy(locator = "btn.pagination.form")
	private QAFExtendedWebElement btnpagination;

	@FindBy(locator = "txt.filter.form")
	private List<WebElement> txtfilter;

	@FindBy(locator = "btn.create.newform")
	private QAFExtendedWebElement btncreate;

	@FindBy(locator = "btn.submite.newform")
	private QAFExtendedWebElement btnsubmit;

	@FindBy(locator = "btn.back.newform")
	private QAFExtendedWebElement btnback;

	@FindBy(locator = "input.title.newpost")
	private QAFExtendedWebElement btntitle;

	@FindBy(locator = "input.posturl.newpost")
	private QAFExtendedWebElement btnposturl;

	@FindBy(locator = "input.imageurl.newpost")
	private QAFExtendedWebElement btnimageurl;

	@FindBy(locator = "input.description.newpost")
	private QAFExtendedWebElement btndescription;

	@FindBy(locator = "select.location.newpost")
	private QAFExtendedWebElement selectlocation;

	@FindBy(locator = "select.category.newpost")
	private QAFExtendedWebElement selectcategory;

	@FindBy(locator = "dropdown.location.newpost")
	private QAFExtendedWebElement dropdownlocation;

	@FindBy(locator = "dropdown.category.newpost")
	private QAFExtendedWebElement dropdowncategory;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public QAFExtendedWebElement getBtnmypost() {
		return btnmypost;
	}

	public QAFExtendedWebElement getBtnpost() {
		return btnpost;
	}

	public List<WebElement> getBtnposttitle() {
		return btnposttitle;
	}

	public QAFExtendedWebElement getBtnsearch() {
		return btnsearch;
	}

	public QAFExtendedWebElement getBtnreset() {
		return btnreset;
	}

	public QAFExtendedWebElement getTxttitle() {
		return txttitle;
	}

	public QAFExtendedWebElement getBtnpagination() {
		return btnpagination;
	}

	public List<WebElement> getTxtfilter() {
		return txtfilter;
	}

	public QAFExtendedWebElement getBtncreate() {
		return btncreate;
	}

	public QAFExtendedWebElement getBtnsubmit() {
		return btnsubmit;
	}

	public QAFExtendedWebElement getBtnback() {
		return btnback;
	}

	public QAFExtendedWebElement getBtnposturl() {
		return btnposturl;
	}

	public QAFExtendedWebElement getBtnimageurl() {
		return btnimageurl;
	}

	public QAFExtendedWebElement getBtntitle() {
		return btntitle;
	}

	public QAFExtendedWebElement getBtndescription() {
		return btndescription;
	}
	
	public QAFExtendedWebElement getSelectlocation() {
		return selectlocation;
	}

	public QAFExtendedWebElement getSelectcategory() {
		return selectcategory;
	}

	public QAFExtendedWebElement getDropdownlocation() {
		return dropdownlocation;
	}

	public QAFExtendedWebElement getDropdowncategory() {
		return dropdowncategory;
	}

	public void redirectToMypost() {
		waitForPageToLoad();

		btnmypost.waitForPresent(40000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnmypost);
		// btnrnr.click();
		// CommonUtils.waitforloaderimg();

		executor.executeScript("arguments[0].click();", btnpost);
		// btnrnrRequest.waitForPresent(40000);
		// btnrnrRequest.click();
		// CommonUtils.waitforloaderimg();
		waitForPageToLoad();
	}

	public void verifyfieldsavailable() {
		// Verify title
		// txttitle.waitForPresent(40000);
		// String titleofpage = txttitle.getText();
		// System.out.println("title of page:"+titleofpage);

		// verify element present on form

		String[] exp = {"Title", "Category", "Posted On", "Action"};

		List<WebElement> UiElements = getBtnposttitle();
		System.out.println("list size: " + UiElements.size());

		for (WebElement we : UiElements) {
			for (int i = 0; i < exp.length; i++) {
				if (we.getText().equals(exp[i])) {
					System.out.println("element " + i + " is Matched with for titles");
				}
			}
		}

		// verify text field available for filter
		String[] exp1 = {"From", "To", "Keyword", "Category"};

		List<WebElement> Elements = getTxtfilter();
		System.out.println("list size: " + Elements.size());

		for (WebElement text : Elements) {
			for (int i = 0; i < exp1.length; i++) {
				if (text.getText().equals(exp1[i])) {
					System.out.println("element " + i + " is Matched for filter");
				}
			}
		}

		// verify search & reset available on page

		Assert.assertEquals(true, btnsearch.isPresent());
		Assert.assertEquals(true, btnreset.isPresent());

		// verify pagination available on page
		Assert.assertEquals(true, btnpagination.isPresent());

		QAFTestBase.pause(30000);
	}

	public void redirectToNewpost() {

		waitForPageToLoad();
		btnmypost.waitForPresent(40000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnmypost);

		executor.executeScript("arguments[0].click();", btnpost);
	
		
		 btncreate.waitForPresent(60000);

		 executor.executeScript("arguments[0].click();", btncreate);
		 
		QAFTestBase.pause(30000);

	}

	public void verifyNewPostPage() {

		waitForPageToLoad();
		// verify title, postUrl, imageUrl, description text fields on new post page
		Assert.assertEquals(true, btntitle.isPresent());
		Assert.assertEquals(true, btnposturl.isPresent());
		Assert.assertEquals(true, btnimageurl.isPresent());
		Assert.assertEquals(true, btndescription.isPresent());
		
		// verify search & reset available on page
		Assert.assertEquals(true, btnsubmit.isPresent());
		Assert.assertEquals(true, btnback.isPresent());
		
	// verify dropdown for location & category
		
		selectlocation.waitForPresent(40000);
		selectlocation.click();
		if(dropdownlocation.isPresent()) {
			System.out.println("dropdown for location is available");
		}
		else
			System.out.println("dropdown for location is not available");
		
		selectcategory.waitForPresent(40000);
		selectcategory.click();
		
		if(dropdowncategory.isPresent()) {
			System.out.println("dropdown for category is available");
		}
		else
			System.out.println("dropdown for category is not available");
	}

}
