package com.nestportal.testpage;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class NominationPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	
	@FindBy(locator = "btn.rnr.navigation")
	private QAFExtendedWebElement btnrnr;
	
	@FindBy(locator = "btn.nominate.navigation")
	private QAFExtendedWebElement btnnominate;
	
	@FindBy(locator = "btn.myrnr.navigation")
	private QAFExtendedWebElement btnmyrnr ;
	
	@FindBy(locator = "btn.nominations.rnr")
	private QAFExtendedWebElement btnnominations ;
		
	@FindBy(locator = "txt.pagetitle.nomination")
	private QAFWebElement titlenominations ;
	
	@FindBy(locator = "txt.breadcrumb.nomination")
	private QAFWebElement txtbreadcrumb ;
	
	@FindBy(locator = "img.headertable.nomination")
	private List<WebElement> imgheadertable ;

	@FindBy(locator = "img.pagination.nomination")
	private QAFExtendedWebElement imgpagination ;

	public QAFExtendedWebElement getBtnnominate() {
		return btnnominate;
	}
	
	public QAFExtendedWebElement getBtnmyrnr() {
		return btnmyrnr;
	}
	
	public QAFExtendedWebElement getBtnnominations() {
		return btnnominations;
	}
	

	public QAFWebElement getTitlenominations() {
		return titlenominations;
	}


	public QAFWebElement getTxtbreadcrumb() {
		return txtbreadcrumb;
	}

	public List<WebElement> getImgheadertable() {
		return imgheadertable;
	}
		
	public QAFExtendedWebElement getImgpagination() {
		return imgpagination;
	}
	
	public void nominate() {
			
        waitForPageToLoad();
		
		btnrnr.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", 	btnrnr);
//		CommonUtils.waitforloaderimg();
//		btnrnr.click();
		
		btnnominate.waitForPresent(40000);
	    executor.executeScript("arguments[0].click();", 	btnmyrnr);
	    
	    btnnominations.waitForPresent(40000);
	    executor.executeScript("arguments[0].click();", 	btnnominations);
	    
	    QAFTestBase.pause(30000);
	}
	
	public void verifyNominatePage() {
        waitForPageToLoad();

        //Verify title of page
     String titleofpage=   titlenominations.getText();
     Assert.assertEquals("My Nominations", titleofpage);
   
     // verify Breadcrumb 
     String Breadcrumbofpage=   txtbreadcrumb.getText();
     Assert.assertEquals("Home /  R & R /  My R & R", Breadcrumbofpage);
     
     // Verify title of table
     
     String[] exp1 = {"Nominee(s)","No. of Reward(s)","Posted Date","Manager Status","HR Status"};
     
     List<WebElement> values =  getImgheadertable();
		System.out.println("list size: "+values.size());
	
		for(WebElement web:values)  
    {  
     for (int i=0; i<exp1.length; i++){
         if (web.getText().equals(exp1[i])){
         System.out.println("element "+i+" is Matched for nomination page");
         } 
       }
    }
	
	//Verify Pagination present
	 Assert.assertEquals(true, imgpagination.isPresent());
	 
	 
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
}
