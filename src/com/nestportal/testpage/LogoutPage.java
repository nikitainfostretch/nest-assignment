package com.nestportal.testpage;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class LogoutPage  extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	@FindBy(locator = "img.logout.Homepage")
	private QAFExtendedWebElement imglogout ;

	
    public QAFExtendedWebElement getImglogout() {
		return imglogout;
	}


  public void logout() throws InterruptedException {
	  try {
		  imglogout.waitForPresent(15000);
		  imglogout.click();
		  waitForPageToLoad();
			  } catch (Exception e) {
			   
			  }
	  
	
	  
	waitForPageToLoad();
	waitForAjaxToComplete();
	
//	Thread.sleep(8000);
	 //driver.close();
 }
	
	
	
	
	
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
