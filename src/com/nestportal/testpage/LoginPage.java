package com.nestportal.testpage;

import org.testng.Assert;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class LoginPage  extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator = "txt.username.login")
	private QAFExtendedWebElement emailField ;

	@FindBy(locator = "txt.password.login")
	private QAFExtendedWebElement passwordField;

	@FindBy(locator = "btn.login.loginpage")
	private QAFExtendedWebElement btnLogin;
	
	@FindBy(locator = "img.logo.HomeScreen")
	private QAFExtendedWebElement imglogo;	

	public QAFExtendedWebElement getEmailField() {
		return emailField;
	}
	
	public QAFExtendedWebElement getPasswordField() {
		return passwordField;
	}

	public QAFExtendedWebElement getBtnLogin() {
		return btnLogin;
	}
	
	public QAFExtendedWebElement getImglogo() {
		return imglogo;
	}
	public void userIsOnLoginPage() throws InterruptedException {
	
		 driver.get("/");
		 driver.manage().window().maximize();
		waitForPageToLoad();
	}
	

	public void doLogin(String username, String password) {
		emailField.sendKeys(ConfigurationManager.getBundle().getString("login.username"));
		passwordField
				.sendKeys(ConfigurationManager.getBundle().getString("login.password"));
		
		btnLogin.waitForPresent(4000);
		btnLogin.submit();

		waitForPageToLoad();
	}
	
	public void Verifylogin() {
		
		try {
			imglogo.waitForPresent(15000);
			  } catch (Exception e) {
			   
			  }
	
		Assert.assertTrue(imglogo.isPresent(),"login is sucessfully!!!");
		waitForPageToLoad();
		
		//driver.close();
		waitForAjaxToComplete();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

	public void Loginwithotherband(String otherusername, String otherpassword) {
		emailField.sendKeys(ConfigurationManager.getBundle().getString("login.otherusername"));
		passwordField
				.sendKeys(ConfigurationManager.getBundle().getString("login.otherpassword"));
		
		btnLogin.waitForPresent(4000);
		btnLogin.submit();
		waitForPageToLoad();	
	}

	


	
	

}
