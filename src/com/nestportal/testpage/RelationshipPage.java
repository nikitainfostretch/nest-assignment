package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class RelationshipPage extends WebDriverBaseTestPage<WebDriverTestPage>  {
	
	@FindBy(locator = "btn.menu.homescreen")
	private QAFExtendedWebElement btnMenu ;
	
	@FindBy(locator = "btn.ess.navigation")
	private QAFExtendedWebElement btnEss ;
	
	@FindBy(locator = "btn.MyProfile.navigation")
	private QAFExtendedWebElement btnMyProfile ;

	@FindBy(locator = "btn.relation.relationpage")
	private QAFExtendedWebElement btnrelation ;
	
	@FindBy(locator = "img.edit.relationpage")
	private QAFExtendedWebElement btnEdit ;
	
	@FindBy(locator = "btn.save.relationpage")
	private QAFExtendedWebElement btnSave ;
	
	public QAFExtendedWebElement getBtnMenu() {
		return btnMenu;
	}

	public QAFExtendedWebElement getBtnEss() {
		return btnEss;
	}

	public QAFExtendedWebElement getBtnMyProfile() {
		return btnMyProfile;
	}

	public QAFExtendedWebElement getBtnrelation() {
		return btnrelation;
	}

	public QAFExtendedWebElement getBtnEdit() {
		return btnEdit;
	}
	
	public QAFExtendedWebElement getBtnSave() {
		return btnSave;
	}

	public void releationship() throws InterruptedException {
		waitForPageToLoad();
		
		btnMenu.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", 	btnMenu);
	
	btnEss.waitForPresent(40000);
	executor.executeScript("arguments[0].click();", 	btnEss);
	
		
	btnMyProfile.waitForPresent(40000);
	executor.executeScript("arguments[0].click();", 	btnMyProfile);
	
	
	CommonUtils.scrollUpToElement(btnrelation);

		btnrelation.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", 	btnrelation);
		
		btnEdit.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", 	btnEdit);
		
		if(btnSave.isPresent()) {
			System.out.println("user is able to click on check button");
		}
		
		QAFTestBase.pause(30000);
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
