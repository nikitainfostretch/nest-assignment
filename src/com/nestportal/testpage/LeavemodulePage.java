package com.nestportal.testpage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.DateUtil;

public class LeavemodulePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "img.leave.leavepage")
	private QAFExtendedWebElement imgleave;

	@FindBy(locator = "btn.leavelist.leavepage")
	private QAFExtendedWebElement btnleavelist;

	@FindBy(locator = "btn.menu.homescreen")
	private QAFExtendedWebElement btnMenu;

	@FindBy(locator = "img.textfield.leavepage")
	private List<WebElement> imgtextfield;

	@FindBy(locator = "btn.submit.leavepage")
	private QAFExtendedWebElement btnsubmit;

	@FindBy(locator = "btn.approveAll.leavepage")
	private QAFExtendedWebElement btnapproveAll;

	@FindBy(locator = "btn.applyleave.leavepage")
	private QAFExtendedWebElement btnapplyleave;

	@FindBy(locator = "input.from.leave")
	private QAFExtendedWebElement inputfrom;

	@FindBy(locator = "input.to.leave")
	private QAFExtendedWebElement inputto;

	@FindBy(locator = "btn.apply.leave")
	private QAFExtendedWebElement btnapply;

	@FindBy(locator = "btn.selectreasoncategory.leave")
	private QAFExtendedWebElement btnselectreasoncategory;

	@FindBy(locator = "select.reason.leave")
	private QAFExtendedWebElement selectreason;

	@FindBy(locator = "select.employee.leave")
	private QAFExtendedWebElement selectemployee;

	@FindBy(locator = "txt.search.leave")
	private QAFExtendedWebElement txtsearch;

	@FindBy(locator = "select.employee.searchedlist")
	private QAFExtendedWebElement selectemployeelist;

	@FindBy(locator = "btn.search.leave")
	private QAFExtendedWebElement btnsearch;

	@FindBy(locator = "btn.select.teamleavelist")
	private QAFExtendedWebElement btnselect;

	@FindBy(locator = "btn.approve.leave")
	private QAFExtendedWebElement btnapprove;

	@FindBy(locator = "btn.leavetype.leave")
	private QAFExtendedWebElement btnleavetype;

	@FindBy(locator = "select.leaveselection.leave")
	private QAFExtendedWebElement selectleaveselection;

	@FindBy(locator = "txt.leaveAppliedError.leave")
	private QAFExtendedWebElement txtleaveAppliedError;

	@FindBy(locator = "btn.leaveType.leave")
	private QAFExtendedWebElement btnleaveType;

	@FindBy(locator = "btn.selectLeaveType.leave")
	private QAFExtendedWebElement btnselectLeaveType;
	
	@FindBy(locator = "btn.appliedLeave.Leave")
	private List<WebElement> btnappliedLeave;

	public QAFExtendedWebElement getImgleave() {
		return imgleave;
	}

	public QAFExtendedWebElement getBtnleavelist() {
		return btnleavelist;
	}

	public QAFExtendedWebElement getBtnMenu() {
		return btnMenu;
	}

	public List<WebElement> getImgtextfield() {
		return imgtextfield;
	}

	public QAFExtendedWebElement getBtnsubmit() {
		return btnsubmit;
	}

	public QAFExtendedWebElement getBtnapproveAll() {
		return btnapproveAll;
	}

	public QAFExtendedWebElement getBtnapplyleave() {
		return btnapplyleave;
	}

	public QAFExtendedWebElement getInputfrom() {
		return inputfrom;
	}

	public QAFExtendedWebElement getInputto() {
		return inputto;
	}

	public QAFExtendedWebElement getBtnapply() {
		return btnapply;
	}

	public QAFExtendedWebElement getBtnselectreasoncategory() {
		return btnselectreasoncategory;
	}

	public QAFExtendedWebElement getSelectreason() {
		return selectreason;
	}

	public QAFExtendedWebElement getSelectemployee() {
		return selectemployee;
	}

	public QAFExtendedWebElement getTxtsearch() {
		return txtsearch;
	}

	public QAFExtendedWebElement getSelectemployeelist() {
		return selectemployeelist;
	}

	public QAFExtendedWebElement getBtnsearch() {
		return btnsearch;
	}

	public QAFExtendedWebElement getBtnselect() {
		return btnselect;
	}

	public QAFExtendedWebElement getBtnapprove() {
		return btnapprove;
	}

	public QAFExtendedWebElement getBtnleavetype() {
		return btnleavetype;
	}

	public QAFExtendedWebElement getSelectleaveselection() {
		return selectleaveselection;
	}

	public QAFExtendedWebElement getTxtleaveAppliedError() {
		return txtleaveAppliedError;
	}

	public QAFExtendedWebElement getBtnleaveType() {
		return btnleaveType;
	}

	public QAFExtendedWebElement getBtnselectLeaveType() {
		return btnselectLeaveType;
	}
	

	public List<WebElement> getBtnappliedLeave() {
		return btnappliedLeave;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public void redirecttoleavelist() {
		waitForPageToLoad();

		btnMenu.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnMenu);

		imgleave.waitForPresent(30000);

		executor.executeScript("arguments[0].click()", imgleave);
		// imgleave.click();

		btnleavelist.waitForPresent(30000);
		executor.executeScript("arguments[0].click()", btnleavelist);
		// btnleavelist.click();

		QAFTestBase.pause(50000);
	}

	public void verifyleave() {
		waitForPageToLoad();

		// Verify ui elements present
		String[] exp1 = {"Employee Name (ID)", "Applied Date", "Type", "Leave Duration",
				"Leave Date", "Status", "Leave Reason", "Project", "Manager's Comment",
				"Actions", "Back Dated Leave"};

		List<WebElement> values = getImgtextfield();
		System.out.println("list size: " + values.size());

		for (WebElement web : values) {
			for (int i = 0; i < exp1.length; i++) {
				if (web.getText().equals(exp1[i])) {
					System.out.println("element " + i + " is Matched for Leave page");
				}
			}
		}

		// Verify submit or approve button
		btnsubmit.waitForPresent(30000);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
				btnsubmit);

		Assert.assertEquals(true, btnsubmit.isPresent());
		// Assert.assertEquals(true, btnapproveAll.isDisplayed());

		QAFTestBase.pause(6000);
	}

	public void redirecttoapplyleave() {
		waitForPageToLoad();

		btnMenu.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnMenu);

		imgleave.waitForPresent(30000);

		executor.executeScript("arguments[0].click()", imgleave);
		// imgleave.click();

		btnapplyleave.waitForPresent(30000);
		executor.executeScript("arguments[0].click()", btnapplyleave);
		// btnleavelist.click();

		QAFTestBase.pause(30000);
	}

	public void applyleave() {

		waitForPageToLoad();

		int leave = 0;

		do {
			leave++;
			Date date = DateUtil.getDate(leave);
			getLeaveDate(date);
		} while (txtleaveAppliedError.isPresent());

		QAFTestBase.pause(30000);
	}

	public void getLeaveDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
		inputfrom.waitForPresent(30000);
		inputfrom.clear();
		inputfrom.sendKeys(sdf.format(date));
		inputto.waitForPresent(30000);
		inputto.clear();
		inputto.sendKeys(sdf.format(date));

		// select reason

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnselectreasoncategory);

		Actions actions = new Actions(driver);

		QAFWebElement moveonmenu = driver.findElement(
				By.xpath("(.//*[@class='dropdown-menu width100per']/li)[2]"));

		moveonmenu.waitForVisible();
		actions.moveToElement(moveonmenu).build().perform();
		moveonmenu.click();

		// scroll till apply button

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				btnapply);

		// Click on apply

		btnapply.waitForPresent(30000);
		// btnapply.click();
		executor.executeScript("arguments[0].click()", btnapply);

		waitForPageToLoad();

		QAFTestBase.pause(6000);

	}

	public void searchEmployeeleavelistPage(String username) {

		waitForPageToLoad();

		selectemployee.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", selectemployee);
		txtsearch.waitForPresent(3000);
		txtsearch.sendKeys(ConfigurationManager.getBundle().getString("Leave.username"));

		selectemployeelist.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", selectemployeelist);
		selectemployee.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", selectemployee);
		btnsearch.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", btnsearch);

		// select action to approve leave
		btnselect.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", btnselect);

		btnapprove.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", btnapprove);

		// btnselect.waitForPresent(3000);
		// executor.executeScript("arguments[0].click();", btnselect);

		btnsubmit.waitForPresent(9000);
		executor.executeScript("arguments[0].click();", btnsubmit);

		QAFTestBase.pause(60000);
	}

	public void applyleaveincludingholiday() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
		Date now = new Date();
		Calendar calendarTo = Calendar.getInstance();
		calendarTo.setTime(now);
		calendarTo.add(Calendar.DAY_OF_MONTH, 7);

		Calendar calendarFrom = Calendar.getInstance();
		calendarFrom.setTime(now);
		calendarFrom.add(Calendar.DAY_OF_MONTH, 1);
		String strDate = sdf.format(calendarFrom.getTime());
		String toDate = sdf.format(calendarTo.getTime());
		inputfrom.sendKeys(strDate);

		waitForPageToLoad();
		inputfrom.clear();
		inputfrom.waitForPresent(9000);
		inputfrom.sendKeys(strDate);
		inputto.clear();
		inputto.waitForPresent(9000);
		inputto.sendKeys(toDate);

		JavascriptExecutor executor = (JavascriptExecutor) driver;

		executor.executeScript("arguments[0].click();", btnselectreasoncategory);

		Actions actions = new Actions(driver);

		QAFWebElement moveonmenu = driver.findElement(
				By.xpath("(.//*[@class='dropdown-menu width100per']/li)[2]"));

		moveonmenu.waitForVisible();
		actions.moveToElement(moveonmenu).build().perform();
		moveonmenu.click();

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				btnapply);
		btnapply.waitForPresent(30000);
		

		QAFTestBase.pause(30000);
	}

	public void verifyleaveIncludingWeekend() {
		//Get leave count
		List<WebElement> LeaveCount = getBtnappliedLeave();
		
		if(LeaveCount.size()==5) {
			System.out.println("Holiday is not counted in applied leave");
		}
		
	}
	
	public void searchEmployeeleavelistPageMultiProejct(String username) {

		waitForPageToLoad();

		selectemployee.waitForPresent(30000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", selectemployee);
		txtsearch.waitForPresent(3000);
		txtsearch.sendKeys(ConfigurationManager.getBundle().getString("Leave.userWithMultiProject"));

		selectemployeelist.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", selectemployeelist);
		selectemployee.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", selectemployee);
		btnsearch.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", btnsearch);

		// select action to approve leave
		btnselect.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", btnselect);

		btnapprove.waitForPresent(3000);
		executor.executeScript("arguments[0].click();", btnapprove);

		// btnselect.waitForPresent(3000);
		// executor.executeScript("arguments[0].click();", btnselect);

		btnsubmit.waitForPresent(9000);
		executor.executeScript("arguments[0].click();", btnsubmit);

		QAFTestBase.pause(60000);
	}

}
