package com.nestportal.testpage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;

import com.nestportal.Utils.CommonUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TravelRequestPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "txt.username.login")
	private QAFExtendedWebElement emailField;

	@FindBy(locator = "txt.password.login")
	private QAFExtendedWebElement passwordField;

	@FindBy(locator = "btn.login.loginpage")
	private QAFExtendedWebElement btnLogin;

	@FindBy(locator = "img.logo.HomeScreen")
	private QAFExtendedWebElement imglogo;

	@FindBy(locator = "btn.travel.homepage")
	private QAFExtendedWebElement btntravel;

	@FindBy(locator = "img.trip.tripdetail")
	private QAFExtendedWebElement imgtripdetail;

	@FindBy(locator = "icon.viewdetail.tripPage")
	private QAFExtendedWebElement tripdetailicon;

	@FindBy(locator = "btn.Approve.trippage")
	private QAFExtendedWebElement btnApprove;

	@FindBy(locator = "btn.reject.trippage")
	private QAFExtendedWebElement btnReject;

	@FindBy(locator = "btn.managerview.HomePage")
	private QAFExtendedWebElement btnmanagerview;

	@FindBy(locator = "btn.viewall.requestpage")
	private QAFExtendedWebElement btnviewall;

	@FindBy(locator = "btn.search.requestpage")
	private QAFExtendedWebElement btnsearch;

	@FindBy(locator = "txt.tripdetail.travel")
	private QAFExtendedWebElement txttripdetail;

	@FindBy(locator = "btn.newtravelrequest.travel")
	private QAFExtendedWebElement btnnewtravelrequest;

	@FindBy(locator = "btn.employeename.travel")
	private QAFExtendedWebElement btnemployeename;

	@FindBy(locator = "intput.search.travel")
	private QAFExtendedWebElement intputsearch;

	@FindBy(locator = "txt.select.travel")
	private QAFExtendedWebElement txtselect;

	@FindBy(locator = "input.travelpurpose.travel")
	private QAFExtendedWebElement inputtravelpurpose;

	@FindBy(locator = "input.projectname.travel")
	private QAFExtendedWebElement inputprojectname;

	@FindBy(locator = "img.cab.travel")
	private QAFExtendedWebElement imgcab;

	@FindBy(locator = "img.hotel.travel")
	private QAFExtendedWebElement imghotel;

	@FindBy(locator = "btn.submit.travel")
	private QAFExtendedWebElement btnsubmit;

	@FindBy(locator = "input.journeyfrom.travel")
	private QAFExtendedWebElement inputjourneyfrom;

	@FindBy(locator = "input.journeyto.travel")
	private QAFExtendedWebElement inputjourneyto;

	@FindBy(locator = "txt.journeyfrom.travel")
	private QAFExtendedWebElement txtjourneyfrom;

	@FindBy(locator = "btn.travelmenu.travel")
	private QAFExtendedWebElement btntravelmenu;

	@FindBy(locator = "btn.travelRequests.travel")
	private QAFExtendedWebElement btntravelRequests;

	@FindBy(locator = "btn.master.travel")
	private QAFExtendedWebElement btnmaster;

	@FindBy(locator = "btn.addNew.travel")
	private QAFExtendedWebElement btnaddNew;

	@FindBy(locator = "input.traveltitle.travel")
	private QAFExtendedWebElement inputtraveltitle;

	@FindBy(locator = "input.level.travel")
	private QAFExtendedWebElement inputlevel;

	@FindBy(locator = "btn.designation.travel")
	private QAFExtendedWebElement btndesignation;

	@FindBy(locator = "btn.modeoftravel.travel")
	private QAFExtendedWebElement btntmodeoftravel;

	@FindBy(locator = "btn.status.travel")
	private QAFExtendedWebElement btnstatus;

	@FindBy(locator = "input.search.travel")
	private QAFExtendedWebElement inputsearch;

	@FindBy(locator = "select.motravel.travel")
	private QAFExtendedWebElement selectmotravel;

	@FindBy(locator = "select.statusofcategory.travel")
	private QAFExtendedWebElement selectstatusofcategory;

	@FindBy(locator = "select.designationcategory.category")
	private QAFExtendedWebElement selectdesignationcategory;

	@FindBy(locator = "btn.edit.category")
	private QAFExtendedWebElement btnedit;

	@FindBy(locator = "btn.delete.category")
	private QAFExtendedWebElement btndelete;

	@FindBy(locator = "btn.yes.category")
	private QAFExtendedWebElement btnyes;

	@FindBy(locator = "btn.modeOfTravelTab.travel")
	private QAFExtendedWebElement btnmodeOfTravelTab;

	@FindBy(locator = "btn.addTravelTitle.modeoftravel")
	private QAFExtendedWebElement btnaddTravelTitle;

	@FindBy(locator = "btn.statusModeoftavel.travel")
	private QAFExtendedWebElement btnstatusModeoftavel;

	@FindBy(locator = "btn.enable.travel")
	private QAFExtendedWebElement btnenable;

	@FindBy(locator = "btn.TravelTab.navigate")
	private QAFExtendedWebElement btnTravelTab;

	@FindBy(locator = "btn.addnewtravel.modeoftravel")
	private QAFExtendedWebElement btnaddnewtravel;
	
	@FindBy(locator = "btn.selectstatus.travel")
	private QAFExtendedWebElement btnselectstatus;
	
	
	@FindBy(locator = "txt.purposeOfTravel.travel")
	private QAFExtendedWebElement txtpurposeOfTravel;

	@FindBy(locator = "txt.typeAdd.travel")
	private QAFExtendedWebElement txttypeAdd;
	
	@FindBy(locator = "txt.journeyFromAdd.travel")
	private QAFExtendedWebElement txtjourneyFromAdd;
	
	@FindBy(locator = "txt.journeyToAdd.travel")
	private QAFExtendedWebElement txtjourneyToAdd;
	
	@FindBy(locator = "txt.journeyStartDateAdd.travel")
	private QAFExtendedWebElement txtjourneyStartDateAdd;
	
	@FindBy(locator = "txt.journeyEndDateAdd.travel")
	private QAFExtendedWebElement txtjourneyEndDateAdd;
	
	@FindBy(locator = "txt.projectNameAdd.travel")
	private QAFExtendedWebElement txtprojectNameAdd;
	
	@FindBy(locator = "input.journeyStartDate.travel")
	private QAFExtendedWebElement inputjourneyStartDate;
	
	@FindBy(locator = "input.journeyEndDate.travel")
	private QAFExtendedWebElement inputjourneyEndDate;
	
	@FindBy(locator = "btn.tripType.travel")
	private QAFExtendedWebElement btntripType;
	
	@FindBy(locator = "btn.selecttripType.travel")
	private QAFExtendedWebElement btnselecttripType;
	
	@FindBy(locator = "btn.selectDate.travel")
	private QAFExtendedWebElement btnselectDate;
	
	@FindBy(locator = "btn.nowOncalender.travel")
	private QAFExtendedWebElement btnnowOncalender;
	
	@FindBy(locator = "btn.close.travel")
	private QAFExtendedWebElement btnclose;
	
	@FindBy(locator = "btn.deleteTravelCategoies.travel")
	private QAFExtendedWebElement btndeleteTravelCategoies;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public QAFExtendedWebElement getEmailField() {
		return emailField;
	}

	public QAFExtendedWebElement getPasswordField() {
		return passwordField;
	}

	public QAFExtendedWebElement getBtnLogin() {
		return btnLogin;
	}

	public QAFExtendedWebElement getImglogo() {
		return imglogo;
	}

	public QAFExtendedWebElement getBtntravel() {
		return btntravel;
	}

	public QAFExtendedWebElement getImgtripdetail() {
		return imgtripdetail;
	}

	public QAFExtendedWebElement getTripdetailicon() {
		return tripdetailicon;
	}

	public QAFExtendedWebElement getBtnApprove() {
		return btnApprove;
	}

	public QAFExtendedWebElement getBtnReject() {
		return btnReject;
	}

	public QAFExtendedWebElement getBtnmanagerview() {
		return btnmanagerview;
	}

	public QAFExtendedWebElement getBtnviewall() {
		return btnviewall;
	}

	public QAFExtendedWebElement getBtnsearch() {
		return btnsearch;
	}

	public QAFExtendedWebElement getTxttripdetail() {
		return txttripdetail;
	}

	public QAFExtendedWebElement getBtnnewtravelrequest() {
		return btnnewtravelrequest;
	}

	public QAFExtendedWebElement getBtnemployeename() {
		return btnemployeename;
	}

	public QAFExtendedWebElement getIntputsearch() {
		return intputsearch;
	}

	public QAFExtendedWebElement getTxtselect() {
		return txtselect;
	}

	public QAFExtendedWebElement getInputtravelpurpose() {
		return inputtravelpurpose;
	}

	public QAFExtendedWebElement getInputprojectname() {
		return inputprojectname;
	}

	public QAFExtendedWebElement getImgcab() {
		return imgcab;
	}

	public QAFExtendedWebElement getImghotel() {
		return imghotel;
	}

	public QAFExtendedWebElement getBtnsubmit() {
		return btnsubmit;
	}

	public QAFExtendedWebElement getInputjourneyfrom() {
		return inputjourneyfrom;
	}

	public QAFExtendedWebElement getInputjourneyto() {
		return inputjourneyto;
	}

	public QAFExtendedWebElement getTxtjourneyfrom() {
		return txtjourneyfrom;
	}

	public QAFExtendedWebElement getBtntravelmenu() {
		return btntravelmenu;
	}

	public QAFExtendedWebElement getBtntravelRequests() {
		return btntravelRequests;
	}

	public QAFExtendedWebElement getBtnmaster() {
		return btnmaster;
	}

	public QAFExtendedWebElement getBtnaddNew() {
		return btnaddNew;
	}

	public QAFExtendedWebElement getInputtraveltitle() {
		return inputtraveltitle;
	}

	public QAFExtendedWebElement getInputlevel() {
		return inputlevel;
	}

	public QAFExtendedWebElement getSelectdesignation() {
		return btndesignation;
	}

	public QAFExtendedWebElement getSelectmodeoftravel() {
		return btntmodeoftravel;
	}

	public QAFExtendedWebElement getSelectstatus() {
		return btnstatus;
	}

	public QAFExtendedWebElement getInputsearch() {
		return inputsearch;
	}

	public QAFExtendedWebElement getBtndesignation() {
		return btndesignation;
	}

	public QAFExtendedWebElement getBtntmodeoftravel() {
		return btntmodeoftravel;
	}

	public QAFExtendedWebElement getBtnstatus() {
		return btnstatus;
	}

	public QAFExtendedWebElement getSelectmotravel() {
		return selectmotravel;
	}

	public QAFExtendedWebElement getSelectstatusofcategory() {
		return selectstatusofcategory;
	}

	public QAFExtendedWebElement getSelectdesignationcategory() {
		return selectdesignationcategory;
	}

	public QAFExtendedWebElement getBtnedit() {
		return btnedit;
	}

	public QAFExtendedWebElement getBtndelete() {
		return btndelete;
	}

	public QAFExtendedWebElement getBtnyes() {
		return btnyes;
	}

	public QAFExtendedWebElement getBtnmodeOfTravelTab() {
		return btnmodeOfTravelTab;
	}

	public QAFExtendedWebElement getBtnaddTravelTitle() {
		return btnaddTravelTitle;
	}

	public QAFExtendedWebElement getBtnstatusModeoftavel() {
		return btnstatusModeoftavel;
	}

	public QAFExtendedWebElement getBtnenable() {
		return btnenable;
	}

	public QAFExtendedWebElement getBtnTravelTab() {
		return btnTravelTab;
	}

	public QAFExtendedWebElement getBtnaddnewtravel() {
		return btnaddnewtravel;
	}

	public QAFExtendedWebElement getBtnselectstatus() {
		return btnselectstatus;
	}
	
	public QAFExtendedWebElement getTxtpurposeOfTravel() {
		return txtpurposeOfTravel;
	}

	public QAFExtendedWebElement getTxttypeAdd() {
		return txttypeAdd;
	}

	public QAFExtendedWebElement getTxtjourneyFromAdd() {
		return txtjourneyFromAdd;
	}

	public QAFExtendedWebElement getTxtjourneyToAdd() {
		return txtjourneyToAdd;
	}

	public QAFExtendedWebElement getTxtjourneyStartDateAdd() {
		return txtjourneyStartDateAdd;
	}

	public QAFExtendedWebElement getTxtjourneyEndDateAdd() {
		return txtjourneyEndDateAdd;
	}

	public QAFExtendedWebElement getTxtprojectNameAdd() {
		return txtprojectNameAdd;
	}
	
	public QAFExtendedWebElement getInputjourneyStartDate() {
		return inputjourneyStartDate;
	}

	public QAFExtendedWebElement getInputjourneyEndDate() {
		return inputjourneyEndDate;
	}
	
	public QAFExtendedWebElement getBtntripType() {
		return btntripType;
	}

	public QAFExtendedWebElement getBtnselecttripType() {
		return btnselecttripType;
	}

	
	public QAFExtendedWebElement getBtnselectDate() {
		return btnselectDate;
	}

	public QAFExtendedWebElement getBtnnowOncalender() {
		return btnnowOncalender;
	}

	public QAFExtendedWebElement getBtnclose() {
		return btnclose;
	}


	public QAFExtendedWebElement getBtndeleteTravelCategoies() {
		return btndeleteTravelCategoies;
	}


	public void doAdminLogin(String adminusername, String adminpassword) {
		emailField.sendKeys(
				ConfigurationManager.getBundle().getString("login.AdminUsername"));
		passwordField.sendKeys(
				ConfigurationManager.getBundle().getString("Login.AdminPassword"));

		btnLogin.waitForPresent(4000);
		btnLogin.submit();
		waitForPageToLoad();

		btnmanagerview.waitForPresent(15000);

		CommonUtils.clickUsingJavaScript(btnmanagerview);
		

	}

	public void travelrequest() {

		btntravel.waitForPresent(15000);
		
		CommonUtils.clickUsingJavaScript(btntravel);
		
		Actions actions = new Actions(driver);

		QAFWebElement moveonmenu = imgtripdetail;
		QAFWebElement subcategory = tripdetailicon;

		moveonmenu.waitForVisible();
		actions.moveToElement(moveonmenu).build().perform();

		subcategory.waitForVisible();

		CommonUtils.clickUsingJavaScript(subcategory);

		CommonUtils.scrollUpToElement(btnApprove);


		if (btnApprove.isEnabled()) {
			Reporter.log("button is enable");
		}

	}

	public void viewrequest() {
		{
			btntravel.waitForPresent(15000);
			CommonUtils.waitforloaderimg();

			CommonUtils.clickUsingJavaScript(btntravel);

			btnviewall.waitForPresent(40000);
			CommonUtils.waitforloaderimg();

			CommonUtils.clickUsingJavaScript(btnviewall);

			btnsearch.waitForPresent(40000);

			if (btnsearch.isPresent()) {
				Reporter.log("User is on view request Page");
			}

		}

	}

	public void redirecttravaldetailpage() {

		btntravelmenu.waitForPresent(15000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btntravelmenu);

		executor.executeScript("arguments[0].click();", btntravelRequests);

		// new travel request button

		CommonUtils.waitforloaderimg();
		executor.executeScript("arguments[0].click();", btnnewtravelrequest);

		waitForPageToLoad();

		QAFTestBase.pause(30000);
	}

	public void fillRequestform(String travelPurpose, String JourneyFrom,
			String JourneyTo, String ProjectName) {

		CommonUtils.waitforloaderimg();

		inputtravelpurpose.sendKeys(
				ConfigurationManager.getBundle().getString("Travel.TravelPurpose"));
		
		//Select tripType
		btntripType.waitForPresent(5000);

		CommonUtils.clickUsingJavaScript(btntripType);
		
		btnselecttripType.waitForPresent(5000);
		
		CommonUtils.clickUsingJavaScript(btnselecttripType);	 
			
		// select journey from
//		inputjourneyfrom.click();

		inputjourneyfrom.sendKeys(
				ConfigurationManager.getBundle().getString("Travel.JourneyFrom"));
		txtjourneyfrom.click();

		// select journey to

//		inputjourneyto.click();
		inputjourneyto
				.sendKeys(ConfigurationManager.getBundle().getString("Travel.JourneyTo"));
		txtjourneyfrom.click();

		// enter project name
		inputprojectname.sendKeys(
				ConfigurationManager.getBundle().getString("Travel.ProjectName"));

		// select cab & hotel booking required

		imgcab.click();
		imghotel.click();

		// select journey date

	        inputjourneyStartDate.waitForVisible(2000);
	        inputjourneyStartDate.click();

	        CommonUtils.clickUsingJavaScript(btnselectDate);	
	        CommonUtils.clickUsingJavaScript(btnnowOncalender);	
	        CommonUtils.clickUsingJavaScript(btnclose);	
	
	       // submit form
		imgcab.submit();

		QAFTestBase.pause(30000);

	}
	

	public void verifyUiOfTravelRequestPage() {

		Assert.assertEquals(true, inputtravelpurpose.isPresent());
		// select journey from
		Assert.assertEquals(true, inputjourneyfrom.isPresent());
		// select journey to
		Assert.assertEquals(true, inputjourneyto.isPresent());
		// enter project name
		Assert.assertEquals(true, inputprojectname.isPresent());

		// select cab & hotel booking required
		Assert.assertEquals(true, imgcab.isPresent());
		Assert.assertEquals(true, imghotel.isPresent());

	}

	public void navigateToMaster() {
		waitForPageToLoad();
		btntravelmenu.waitForPresent(15000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btntravelmenu);
		executor.executeScript("arguments[0].click();", btnmaster);

		waitForPageToLoad();

	}

	public void addTravelCategory(String Title,String Level,String AddDesignation) {
		waitForPageToLoad();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	
		// click on add new
		executor.executeScript("arguments[0].click();", btnaddNew);

		CommonUtils.waitforloaderimg();

		inputtraveltitle.sendKeys(ConfigurationManager.getBundle().getString("title.AddTravelCategory"));

		inputlevel.sendKeys(ConfigurationManager.getBundle().getString("level.AddTravelCategory"));
		
		btndesignation.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", btndesignation);
		
		inputsearch.waitForVisible(50000);		
		inputsearch.sendKeys(ConfigurationManager.getBundle().getString("designation.AddTravelCategory"));
		
		selectdesignationcategory.waitForPresent(40000);

		executor.executeScript("arguments[0].click();", selectdesignationcategory);
		
		btndesignation.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", btndesignation);

		executor.executeScript("arguments[0].click();", btntmodeoftravel);
		selectmotravel.waitForPresent(30000);
		executor.executeScript("arguments[0].click();", selectmotravel);

		executor.executeScript("arguments[0].click();", btnstatus);

		executor.executeScript("arguments[0].click();", selectstatusofcategory);

		inputlevel.submit();

		QAFTestBase.pause(30000);

	}

	public void updateTravelCategory(String title) {
		// click on Edit
		btnedit.click();
		// update category name
		inputtraveltitle.sendKeys(ConfigurationManager.getBundle().getString("updateTitle.AddTravelCategory"));
		// click to submit
		inputlevel.waitForPresent(15000);
		inputlevel.submit();
		waitForPageToLoad();
	}

	public void deleteTravelCategory() {
		waitForPageToLoad();

		btndelete.waitForPresent(40000);
		// click on delete
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btndelete);

		// tab on submit
		btnyes.waitForPresent(40000);
		executor.executeScript("arguments[0].click();", btnyes);

		CommonUtils.waitforloaderimg();

		
		QAFTestBase.pause(50000);

	}

	public void verifyAsteriskSignFields() {
		//Verify Asterisk sign for Purpose of Travel,Type,Journey From,Journey To,Journey Start Date,Journey End Date,Project Name.		


		Assert.assertEquals(true, txtpurposeOfTravel.isPresent());

		Assert.assertEquals(true, txttypeAdd.isPresent());

		Assert.assertEquals(true, txtjourneyFromAdd.isPresent());

		Assert.assertEquals(true, txtjourneyToAdd.isPresent());

		Assert.assertEquals(true, txtjourneyStartDateAdd.isPresent());

		Assert.assertEquals(true, txtjourneyEndDateAdd.isPresent());

		Assert.assertEquals(true, txtprojectNameAdd.isPresent());
		
		
	}


}
