package com.nestportal.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CommonUtils {


	public static  String LOADER = "wait.loader";
    WebDriverWait wait = new WebDriverWait(getDriver(), 30);

	
	public static QAFExtendedWebDriver getDriver() {
		return new WebDriverTestBase().getDriver();
	}

	public  WebElement waitForElementToBeClickable(QAFWebElement ele) {
		return wait.until(ExpectedConditions.elementToBeClickable(ele));
	}

	public  boolean textToBePresentInElement(QAFWebElement ele, String text) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 2, 200);
		return wait.until(ExpectedConditions.textToBePresentInElement(ele, text));
	}

	public static  void clickUsingJavaScript(WebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", ele);
	}

	public static void scrollUpToElement(QAFWebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView()", ele);
	}
//	public  void waitForLoader() {
//		waitForNotVisible(LOADER, 60);
//		CommonPage.waitForLocToBeClickable("button.logout.userhomepage");
//	}
//
//	public void waitForLoaderWithoutCondition() {
//		waitForNotVisible(LOADER, 60);
//	}
	

	public static void clickUsingAction(String loc) {
		Actions builder = new Actions(getDriver());
		builder.moveToElement(new QAFExtendedWebElement(loc)).perform();
		builder.moveToElement(new QAFExtendedWebElement(loc)).click().perform();
	}

	public static  void clickUsingActionOnElement(QAFWebElement element) {
		Actions builder = new Actions(getDriver());
		builder.moveToElement(element).build();
		builder.moveToElement(element).click().perform();
	}
	
	
	public static  void waitforloaderimg() {
			
		try {
			
			QAFWebElement element = new QAFExtendedWebElement(By.cssSelector("img.loader.homepage"));
			element.waitForNotPresent();
		
		}catch (Exception e) {
		
			e.printStackTrace();
		}

	}
	

	
}

	

