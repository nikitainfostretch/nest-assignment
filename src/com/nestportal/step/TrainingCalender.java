package com.nestportal.step;

import com.nestportal.testpage.TrainingCalenderPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class TrainingCalender {


	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect to training detail page")
	public void userRedirectToTainingDetailPage() throws InterruptedException {
		// TODO: remove NotYetImplementedException and call test steps
		TrainingCalenderPage test = new TrainingCalenderPage();
		test.redirectToTrainingPage();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user should able to register for training")
	public void userShouldAbleToRegisterForTraining() {
		// TODO: remove NotYetImplementedException and call test steps
		TrainingCalenderPage test = new TrainingCalenderPage();
		test.registerForTraining();
	}
	
	@QAFTestStep(description = "user should able to deregister for training")
	public void userShouldAbleToDeRegisterForTraining() {
		// TODO: remove NotYetImplementedException and call test steps
		TrainingCalenderPage test = new TrainingCalenderPage();
		test.registerForTraining();
	}

}
