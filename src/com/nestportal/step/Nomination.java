package com.nestportal.step;


import com.nestportal.testpage.NominationPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class Nomination {
	@QAFTestStep(description = "user redirect to nomination page")
	public void userRedirectToNominationPage() {
		NominationPage test = new NominationPage();
		test.nominate();
	}
	
	@QAFTestStep(description = "verify nomination Page")
	public void VerifyPage() {
		NominationPage test = new NominationPage();
		test.verifyNominatePage();
	}
}
