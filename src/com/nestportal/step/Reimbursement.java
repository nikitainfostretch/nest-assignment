package com.nestportal.step;

import com.nestportal.testpage.ReimbursementPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class Reimbursement {

	@QAFTestStep(description = "user redirect to expense page")
	public void userRedirectToleavepage() {
		ReimbursementPage test = new ReimbursementPage();
		test.redirecttomyexpense();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user upload file")
	public void userUploadFile() {
		ReimbursementPage test = new ReimbursementPage();
		test.uploadfile();
	}

	@QAFTestStep(description = "verify upload")
	public void verifyupload() {
		ReimbursementPage test = new ReimbursementPage();
		test.verifyupload();
	}


	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect to Team Reimbursement List")
	public void userRedirectToTeamReimbursementList() {
		ReimbursementPage test = new ReimbursementPage();
		test.teamReimbursementPage();
	}


	@QAFTestStep(description = "user fill the form with Title {0},project{1},projectName{2},expenseTitle{3},expenseAmount{4}")
	public void userFillTheFormWithdetails(
			String title, String project, String projectname,
			String expenseTitle, int expenseAmount) {
		ReimbursementPage test = new ReimbursementPage();
		test.fillexpenseform(title, project, projectname, expenseTitle, expenseAmount);
	}

}
