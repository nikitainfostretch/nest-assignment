package com.nestportal.step;

import com.nestportal.testpage.TrainingModulePage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class TrainingModule {

	@QAFTestStep(description = "navigate to training detail page")
	public void navigateToTrainingDetailPage() {
		TrainingModulePage test = new TrainingModulePage();
		test.redirecttotraining();
	}

	@QAFTestStep(description = "user browse the file")
	public void userBrowseTheFile() {
		TrainingModulePage test = new TrainingModulePage();
		test.browsefile();
	}

}
