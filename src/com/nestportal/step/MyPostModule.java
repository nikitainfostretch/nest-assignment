package com.nestportal.step;

import com.nestportal.testpage.MyPostPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class MyPostModule {

	@QAFTestStep(description = "user redirect to My post page")
	public void userRedirectToMyPostPage() {
		MyPostPage test = new MyPostPage();
		test.redirectToMypost();
	}

	@QAFTestStep(description = "verify elements on My post page")
	public void verifyElementsOnMyPostPage() {
		MyPostPage test = new MyPostPage();
		test.verifyfieldsavailable();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect to new post page")
	public void userRedirectToNewPostPage() {
		MyPostPage test = new MyPostPage();
		test.redirectToNewpost();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "verify new post page")
	public void verifyNewPostPage() {
		MyPostPage test = new MyPostPage();
		test.verifyNewPostPage();
	}

}
