package com.nestportal.step;

import com.nestportal.testpage.BrightSparkPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class BrightSpark {
	@QAFTestStep(description = "user redirect to bright spark award")
	public void brightspark() {
		BrightSparkPage test = new BrightSparkPage();
		test.brightspark();
	}

	@QAFTestStep(description = "user redirect to RnR request")
	public void userRedirectToRnr() {
		BrightSparkPage test = new BrightSparkPage();
		test.rnrPage();
	}

	@QAFTestStep(description = "user redirect to Bright Spark where status is Rejected ")
	public void userRedirectToBrightSparkWhereStatusIsRejected() {
		BrightSparkPage test = new BrightSparkPage();
		test.applyFilter();
	}

	@QAFTestStep(description = "Verify UI of bright spark page")
	public void VerifyUIofbrightsparkpage() {
		BrightSparkPage test = new BrightSparkPage();
		test.verifyPageDetail();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect to My reward")
	public void userRedirectToMyReward() {
		BrightSparkPage test = new BrightSparkPage();
		test.redirecttomyreward();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect to nominee award detail screen")
	public void userRedirectToNomineeAwardDetailScreen() {
		BrightSparkPage test = new BrightSparkPage();
		test.redirectToAwardDetailScreen();
	}

	@QAFTestStep(description = "user submite deatils of nominee{0},projectname{1},inputsituation{2},inputsolution{3},inputbenefit{4},inputRational{5}")
	public void userSubmiteDeatilsOfNomineeProjectnameInputsituationInputsolutionInputbenefitInputRational(
			String nomineeName, String projectName, String inputsituation,
			String inputsolution, String inputbenefit, String inputRational) {
		BrightSparkPage test = new BrightSparkPage();
		test.SubmitDetails(nomineeName, projectName, inputsituation, inputsolution,
				inputbenefit, inputRational);
	}

}
