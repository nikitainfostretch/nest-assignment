package com.nestportal.step;

import com.nestportal.testpage.InvalidLoginPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class InvalidLogin {

	

	@QAFTestStep(description = "click on login Button")
	public void launch() {
		InvalidLoginPage test = new InvalidLoginPage();
		test.doInvalidLogin();
	}

	@QAFTestStep(description = "Verify alert text on login")
	public void verifyAlerttext() {
		InvalidLoginPage test1 = new InvalidLoginPage();
		test1.verifyAlerttext();
	}


	

}
