package com.nestportal.step;

import com.nestportal.testpage.ReportsPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class Reports {

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect to Reports")
	public void userRedirectToReports() {
		ReportsPage test = new ReportsPage();
		test.redirectToReport();
	}

}
