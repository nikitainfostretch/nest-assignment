package com.nestportal.step;

import com.nestportal.testpage.LogoutPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class Logout {
	
	
	@QAFTestStep(description = "logout form web portal")
	public void logout()throws InterruptedException{
		
		LogoutPage log = new LogoutPage();
		log.logout();
		
	}
}
