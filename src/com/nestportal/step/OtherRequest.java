package com.nestportal.step;


import com.nestportal.testpage.OtherRequestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class OtherRequest {
	@QAFTestStep(description = "user redirect to other request section")
	public void otherrequest() throws InterruptedException {
		OtherRequestPage testt = new OtherRequestPage();
		testt.otherrequest();
	}
	
	@QAFTestStep(description = "search nominated brightspark employee{0}")
	public void searchNomination(String nomineeName) {
		OtherRequestPage testt = new OtherRequestPage();
		testt.searchNomination(nomineeName);
	}
	
	@QAFTestStep(description = "Verify user is present on  manager login")
	public void verifyUserPresent() {
		OtherRequestPage testt = new OtherRequestPage();
		testt.verifyUserPresent();
	}
	
}
