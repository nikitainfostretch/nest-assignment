package com.nestportal.step;

import com.nestportal.testpage.PatOnBackPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class PatOnBack {

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirect on pat on back award")
	public void userRedirectOnPatOnBackAward() {
		PatOnBackPage test = new PatOnBackPage();
		test.redirectonpatonback();

	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user submit patonback form with nominee{0},projectname{1}, addContribution{2}")
	public void userSubmitPatonbackForm(String nominee,String projectname,String addContribution) {
		PatOnBackPage test = new PatOnBackPage();
		test.submitPatOnBackForm(nominee,projectname,addContribution);
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "search nominated patonback employee{0}")
	public void searchNominatedPatonbackEmployee(String nominee) {
		PatOnBackPage test = new PatOnBackPage();
		test.searchPatOnBackUser(nominee);
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "verify user present in nomination of patonback")
	public void verifyUserPresentInNominationOfPatonback() {
		PatOnBackPage test = new PatOnBackPage();
		test.verifyPatOnBackUser();
	}

}
