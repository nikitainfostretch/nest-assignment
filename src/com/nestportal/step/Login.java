package com.nestportal.step;

import com.nestportal.testpage.LoginPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class Login {

	@QAFTestStep(description = "user is on login screen")
	public void userIsOnLoginPage() throws InterruptedException {
		LoginPage testt = new LoginPage();
		testt.userIsOnLoginPage();
	}

	@QAFTestStep(description = "user login with username {0} and password {1}")
	public void doLogin(String username, String password) {
		LoginPage test = new LoginPage();
		test.doLogin(username, password);
	}

	@QAFTestStep(description = "Verify user is on Home Screen")
	public void Verifylogin() {
		LoginPage test1 = new LoginPage();
		test1.Verifylogin();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user login with other username {0} and password {1}")
	public void userLoginWithOtherUsernameAndPassword(String otherusername, String otherpassword) {
		LoginPage test2 = new LoginPage();
		test2.Loginwithotherband(otherusername,otherpassword);
	}

}
