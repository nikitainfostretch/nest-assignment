package com.nestportal.step;

import com.nestportal.testpage.TravelRequestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class TravelRequest {

	@QAFTestStep(description = "Admin login with username {0} and password {1}")
	public void adminLoginWithUsernameAndPassword(String adminusername,
			String adminpassword) {
		TravelRequestPage test = new TravelRequestPage();
		test.doAdminLogin(adminusername, adminpassword);
	}

	@QAFTestStep(description = "Approve travel request")
	public void Approverequest() {
		TravelRequestPage testt = new TravelRequestPage();
		testt.travelrequest();
	}

	@QAFTestStep(description = "view request for travel")
	public void viewrequest() {
		TravelRequestPage testt1 = new TravelRequestPage();
		testt1.viewrequest();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirected to travel detail page")
	public void userRedirectedToTravelDetailPage() {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.redirecttravaldetailpage();

	}


	
	@QAFTestStep(description = "user fill travel request form with travelPurpose{0},JourneyFrom{1},JourneyTo{2},ProjectName{3}")
	public void userFillTravelRequestForm(
			String travelPurpose, String JourneyFrom,
			String JourneyTo, String ProjectName) {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.fillRequestform(travelPurpose, JourneyFrom, JourneyTo, ProjectName);
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "text field having asterisk sign")
	public void textFieldHavingAsteriskSign() {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.verifyAsteriskSignFields();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "verify Ui of travel request page")
	public void verifyUiOfTravelRequestPage() {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.verifyUiOfTravelRequestPage();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user redirected to master")
	public void userRedirectedToMaster() {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.navigateToMaster();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "Add travel category with title{0},level{1},AddDesignation{2}")
	public void addTravelCategory(String Title,String Level,String AddDesignation) {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.addTravelCategory(Title, Level, AddDesignation);
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "User update category detail{0}")
	public void userUpdateCategoryDetail(String title) {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.updateTravelCategory( title);

	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "Delete added cateory")
	public void deleteAddedCateory() {
		TravelRequestPage test1 = new TravelRequestPage();
		test1.deleteTravelCategory();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */

}
