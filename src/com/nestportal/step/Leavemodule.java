package com.nestportal.step;

import com.nestportal.testpage.LeavemodulePage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class Leavemodule {

	@QAFTestStep(description = "User redirect to leave module page")
	public void userRedirectToLeaveModulePage() {
		// TODO: remove NotYetImplementedException and call test steps
		LeavemodulePage test = new LeavemodulePage();
		test.redirecttoleavelist();
	}

	@QAFTestStep(description = "verify leave Page")
	public void verifyleavepage() {
		LeavemodulePage test = new LeavemodulePage();
		test.verifyleave();
	}

	@QAFTestStep(description = "User redirect to Apply leave")
	public void applyLeavepage() {
		LeavemodulePage test = new LeavemodulePage();
		test.redirecttoapplyleave();
	}

	@QAFTestStep(description = "user fill apply leave form")
	public void userFillApplyLeaveForm() {
		LeavemodulePage test = new LeavemodulePage();
		test.applyleave();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "User fill leave form including holiday")
	public void userRedirectToApplyLeaveIncludingHoliday() {
		LeavemodulePage test = new LeavemodulePage();
		test.applyleaveincludingholiday();

	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user approve leave for user{0}")
	public void userApproveLeaveForUser(String username) {
		LeavemodulePage test = new LeavemodulePage();
		test.searchEmployeeleavelistPage(username);
	}

	

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "Verify leaves")
	public void verifyLeaves() {
		LeavemodulePage test = new LeavemodulePage();
		test.verifyleaveIncludingWeekend();
	}

	/**
	 * Auto-generated code snippet by QMetry Automation Framework.
	 */
	@QAFTestStep(description = "user approve leave for user having multiple projects{0}")
	public void userApproveLeaveForUserHavingMultipleProjects(String username) {
		LeavemodulePage test = new LeavemodulePage();
		test.searchEmployeeleavelistPageMultiProejct(username);
	}
	
	
}
